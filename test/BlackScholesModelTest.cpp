#include "Parser.hpp"
#include "BlackScholesModel.hpp"

#include "gtest/gtest.h"

using namespace std;

class BlackScholesModelTest : public ::testing::Test {
    protected:

        virtual void SetUp() {
            // code called before each test (beginning)
        }

        virtual void TearDown() {
            // code called before each test destructor (ending)
        }
};

/* TEMPS T = 0 */

TEST_F(BlackScholesModelTest, Asian_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    string optionType;
    int nbTimeSteps;
    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/data-soutenance/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->asset(path, T, nbTimeSteps, rng);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}

TEST_F(BlackScholesModelTest, BasketOption_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    int nbTimeSteps;
    string optionType;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->asset(path, T, nbTimeSteps, rng);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}

TEST_F(BlackScholesModelTest, BasketOption1_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    int nbTimeSteps;
    string optionType;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_1.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->asset(path, T, nbTimeSteps, rng);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}

TEST_F(BlackScholesModelTest, BasketOption2_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    int nbTimeSteps;
    string optionType;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_2.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->asset(path, T, nbTimeSteps, rng);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}

TEST_F(BlackScholesModelTest, Perf_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    string optionType;
    int nbTimeSteps;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/perf.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->asset(path, T, nbTimeSteps, rng);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}

/* TEMPS T = t */

TEST_F(BlackScholesModelTest, Asian_t_eq_0) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    int nbTimeSteps;
    string optionType;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* PAST */

    PnlMat *past = pnl_mat_create(1, size);
    pnl_mat_set_row(past, spot, 0);

    //cout << endl << "=== PAST ===" << endl;
    //pnl_mat_print(past);

    /* PATH */

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs2 = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs2->asset(path, 0, T, nbTimeSteps, rng, past);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&past);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs2;
}

TEST_F(BlackScholesModelTest, Basket_t) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;
    int nbTimeSteps;
    string optionType;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* PAST */

    PnlMat *past = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs1 = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs1->asset(past, T, nbTimeSteps, rng);

    //cout << endl << "=== PAST ===" << endl;
    //pnl_mat_print(past);

    /* PATH */

    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *bs2 = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs2->asset(path, 0, T, nbTimeSteps, rng, past);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&past);
    pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs2;
    delete bs1;
}

TEST_F(BlackScholesModelTest, Asian_simul_market) {
    // Initialisation
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T; /// maturité de l'option
    string optionType; /// type de l'option (basket, asian, performance)
    int nbTimeSteps;

    double strike;
    PnlVect *coefs;
    size_t nbSamples;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    PnlMat *market = pnl_mat_create(H, size);

    BlackScholesModel *bs = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    bs->simul_market(market, T, H, rng);

    /*cout << endl << "=== MARKET ===" << endl;
      pnl_mat_print(market);*/
    

    // Free
    pnl_rng_free(&rng);
    pnl_mat_free(&market);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete bs;
}
