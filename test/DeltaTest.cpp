#include "Parser.hpp"
#include "MonteCarlo.hpp"
#include "BasketOption.hpp"
#include "AsianOption.hpp"
#include "PerformanceOption.hpp"
#include "BlackScholesModel.hpp"

#include "gtest/gtest.h"

using namespace std;

class DeltaTest : public ::testing::Test {
protected:

    virtual void SetUp() {
        // code called before each test (beginning)
    }

    virtual void TearDown() {
        // code called before each test destructor (ending)
    }
};

TEST_F(DeltaTest, DISABLED_ShiftAsset) {
    // Initialisation
    int size = 3; /// nombre d'actifs du modèle
    int nbTimeSteps = 9; /// nombre d'unités de temps
    BlackScholesModel *bs = new BlackScholesModel();

    // Remplissage de path
    PnlMat *path = pnl_mat_create(nbTimeSteps, size);
    for (int i = 0; i < nbTimeSteps; ++i) {
        for (int j = 0; j < size; ++j) {
            MLET(path, i, j) = i + 1;
        }
    }

    // Création de shift_path
    PnlMat *shift_path = pnl_mat_create(nbTimeSteps, size);
    bs->shiftAsset(shift_path, path, 1, 0.5, 7, 1);

    // Test
    for (int i = 0; i < nbTimeSteps; ++i) {
        for (int j = 0; j < size; ++j) {
            if ((i == 7) && (j == 1)) {
                ASSERT_EQ(12, MGET(shift_path, i, j));
            }
            else if ((i == 8) && (j == 1)) {
                ASSERT_EQ(13.5, MGET(shift_path, i, j));
            }
            else {
                ASSERT_EQ(i + 1, MGET(shift_path, i, j));
            }
        }
    }
    
    // Free
    pnl_mat_free(&path);
    pnl_mat_free(&shift_path);
    
    delete bs;
}

TEST_F(DeltaTest, PROF) {
    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/data-soutenance/perf.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);
    
    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    PerformanceOption *bo = new PerformanceOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    // Calcul des delta
    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);
    mod->asset(path, 0, nbTimeSteps, rng);
    
    PnlVect *delta = pnl_vect_create(size);
    mc->delta(path, 0, delta);

    double deltaArray[] = {
        0.0162094, 0.01621, 0.01622, 0.0162203, 0.0162145,
        0.0162044, 0.016215, 0.0162163, 0.0161995, 0.0161993,
        0.0162085, 0.0161999, 0.0161848, 0.0161824, 0.0162018,
        0.0161994, 0.0161971, 0.0162066, 0.0162119, 0.0162086,
        0.0162088, 0.0162014, 0.0162149, 0.0162236, 0.0162311,
        0.0162138, 0.0162064, 0.0162129, 0.0162166, 0.0162183,
        0.0161925, 0.0162276, 0.016189, 0.0161958, 0.0162017,
        0.0162142, 0.0162028, 0.0162092, 0.0162104, 0.0162025
    };

    PnlVect *trueDelta = pnl_vect_create_from_ptr(size, deltaArray);

    // Test d'égalité
    /*for (int i = 0; i < size; ++i) {
        ASSERT_NEAR(GET(delta, i), GET(trueDelta, i), 10e-4);
    }*/
    
    cout << "DELTAS" << endl;
    pnl_vect_print_asrow(delta);

    // Free
    pnl_rng_free(&rng);

    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&delta);
    pnl_vect_free(&trueDelta);
    pnl_vect_free(&coefs);

    pnl_mat_free(&path);

    delete P;
    delete mc;
    delete mod;
    delete bo;
}

TEST_F(DeltaTest, DISABLED_Basket_3) {
    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_3.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    BasketOption *bo = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    // Calcul des delta
    PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);
    mod->asset(path, 0, nbTimeSteps, rng);
    
    PnlVect *delta = pnl_vect_create(size);
    mc->delta(path, 0, delta);

    double deltaArray[] = {
        0.0248956, 0.0249324, 0.0249503, 0.0250191, 0.0249342,
        0.024961, 0.0249808, 0.0248986, 0.0249032, 0.0249421,
        0.0249554, 0.0249416, 0.0249236, 0.0249541, 0.0249515,
        0.0250078, 0.0248439, 0.0248783, 0.0249421, 0.0249178,
        0.0249168, 0.0249511, 0.0249393, 0.0250007, 0.0249344,
        0.0248973, 0.0249136, 0.0249047, 0.024927, 0.0248953,
        0.0249739, 0.024962, 0.0249467, 0.0249322, 0.0249571,
        0.0249858, 0.0248853, 0.024914, 0.024861, 0.0250143
    };

    PnlVect *trueDelta = pnl_vect_create_from_ptr(size, deltaArray);

    // Test d'égalité
    for (int i = 0; i < size; ++i) {
        ASSERT_NEAR(GET(delta, i), GET(trueDelta, i), 10e-4);
    }

    // Free
    pnl_rng_free(&rng);

    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&delta);
    pnl_vect_free(&trueDelta);
    pnl_vect_free(&coefs);

    pnl_mat_free(&path);

    delete P;
    delete mc;
    delete mod;
    delete bo;
}
