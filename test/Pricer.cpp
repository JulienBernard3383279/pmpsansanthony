#include "Parser.hpp"
#include "Option.hpp"
#include "MonteCarlo.hpp"
#include "Option.hpp"
#include "BasketOption.hpp"
#include "AsianOption.hpp"
#include "PerformanceOption.hpp"
#include "BlackScholesModel.hpp"
#include "PandL.hpp"

using namespace std;

int main (int argc, char *argv[]) {
    /* TEMPS DE CALCUL */
    double calcTime;
    clock_t start, end;

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Option de l'exécutable
    string exeOption;

    // Parser
    Param *P;

    /* Lecture des arguments */
    if (argc == 2) {
        string data_input = argv[1];
        P = new Parser(data_input.c_str());

    }
    else if (argc > 2) {
        exeOption = argv[1];
        if (exeOption == "-c") {
            string data_input = argv[3];
            P = new Parser(data_input.c_str());

        }
        else {
            // Arrêt du programme
            cout << argv[1] << endl;
            cout << "Utilisation : ./parser [-c] [market_file] data_input" << endl;
            return EXIT_FAILURE;
        }
    }

    exeOption = argv[1];

    /* Parsing */
    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    if (optionType != "performance") { P->extract("strike", strike); }
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /* Caractéristiques de l'option */
    cout << endl << "option size\t\t\t" << size << endl;
    if (optionType != "performance") { cout << "strike\t\t\t\t" << strike << endl; }
    cout << "spot\t\t\t\t";
    pnl_vect_print_asrow(spot);
    cout << "maturity\t\t\t" << T << endl;
    cout << "volatility\t\t\t";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate\t\t\t" << r << endl;
    cout << "correlation\t\t\t" << rho << endl;
    cout << "trend\t\t\t\t";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type\t\t\t" << optionType << endl;
    cout << "payoff coefficients\t\t";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number\t\t\t" << nbTimeSteps << endl;
    cout << "sample number\t\t\t" << nbSamples << endl;
    cout << "hedging dates number\t\t" << H << endl;

    /* BS Model en t = 0 */
    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    /* Création de path */
    PnlMat *path;

    if (exeOption == "-c") {
        path = pnl_mat_create_from_file(argv[2]);
    } else {
        path = pnl_mat_create(nbTimeSteps+1, size);
        mod->asset(path, 0, nbTimeSteps, rng);
    }
    // MonteCarlo
    MonteCarlo *mc;
    Option *opt;
    
    /* Construction de l'option */
    if (optionType == "basket") {
        opt = new BasketOption(T, nbTimeSteps, size, coefs, strike);
    }
    else if (optionType == "asian") {
        opt = new AsianOption(T, nbTimeSteps, size, coefs, strike);
    }
    else if (optionType == "performance") {
        opt = new PerformanceOption(T, nbTimeSteps, size, coefs, strike);
    }
    else {
        cerr << "Erreur sur le type de l'option" << endl;
        return EXIT_FAILURE;
    }
    
    mc = new MonteCarlo(mod, opt, rng, fdStep, nbSamples);

    double prix = 0, ic = 0;
    
    mod->asset(path, 0, nbTimeSteps, rng);

    /* Calcul du prix et des deltas en t = 0 */
    cout << endl << "***** Valeurs calculées en t = 0 *****" << endl;
    
    PnlVect *delta = pnl_vect_create(size);

    start = clock();
    mc->price(prix, ic); // prix
    mc->delta(path, 0, delta); // deltas
    end = clock();

    cout << "Prix de l'option :\t\t" << prix << endl;
    cout << "Intervalle de confiance :\t" << ic << endl;

    cout << "Valeur des deltas :\t\t";
    pnl_vect_print_asrow(delta);

    calcTime = (double)(end-start)*1000/CLOCKS_PER_SEC;
    cout << "Temps de calcul :\t\t" << calcTime << " ms" << endl << endl;

    /* Calcul du P&L */
    if (exeOption == "-c") {
        double calcTime2;
        clock_t start2, end2;
        PnlVect *pf = pnl_vect_create(H);
        PandL *pandl = new PandL();
        double error;

        start2 = clock();
        pandl->hedgingPortfolio(path, mc, pf, error);
        end2 = clock();

        cout << endl << "Valeur du P&L :\t\t\t" << error << endl;

        calcTime2 = (double)(end2-start2)/CLOCKS_PER_SEC;
        cout << "Temps de calcul :\t\t" << calcTime2 << " s" << endl << endl;

        pnl_vect_free(&pf);
        delete pandl;
    }

    pnl_rng_free(&rng);

    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    pnl_vect_free(&delta);

    pnl_mat_free(&path);

    delete P;
    delete mc;
    delete mod;
    delete opt;

    return EXIT_SUCCESS;
}
