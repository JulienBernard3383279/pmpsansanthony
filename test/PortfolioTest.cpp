#include "Parser.hpp"

#include "PandL.hpp"
#include "MonteCarlo.hpp"
#include "BasketOption.hpp"
#include "AsianOption.hpp"
#include "BlackScholesModel.hpp"

#include "gtest/gtest.h"

using namespace std;

class PortfolioTest: public ::testing::Test {
    protected:

        virtual void SetUp() {
            // code called before each test (beginning)
        }

        virtual void TearDown() {
            // code called before each test destructor (ending)
        }
};


TEST_F(PortfolioTest, DISABLED_Basket2) {
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_2.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);
    PnlMat *givenTraj = pnl_mat_create(H+1, size);

    mod->asset(givenTraj, T, H, rng);

    BasketOption *bo = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    double price, ic = 0;
    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    double error=0;
    PnlVect *pf = pnl_vect_create(H);

    PandL *pandl = new PandL();
    pandl->hedgingPortfolio(givenTraj, mc, pf, error);
    cout << endl << "error = " << error << endl;

    ASSERT_TRUE(error > -5 && error < 5);

    // Free
    pnl_rng_free(&rng);

    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    pnl_vect_free(&pf);
    pnl_mat_free(&givenTraj);
}

TEST_F(PortfolioTest, PROF) {
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/data-soutenance/call.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);
    PnlMat *givenTraj = pnl_mat_create(H+1, size);

    mod->asset(givenTraj, T, H, rng);

    BasketOption *ao = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    double price, ic = 0;
    MonteCarlo *mc = new MonteCarlo(mod, ao, rng, fdStep, nbSamples);

    double error=0;
    PnlVect *pf = pnl_vect_create(H);

    PandL *pandl = new PandL();
    pandl->hedgingPortfolio(givenTraj, mc, pf, error);
    cout << endl << "error = " << error << endl;

    ASSERT_TRUE(error > -2 && error < 2);

    pnl_rng_free(&rng);
    pnl_vect_free(&pf);
    pnl_mat_free(&givenTraj);
}

TEST_F(PortfolioTest, DISABLED_Simul_Asian) {
    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    // Parsing du fichier
    string infile1 = "../../data/simul_asian.dat";
    PnlMat *givenTraj = pnl_mat_create_from_file(infile1.c_str());

    AsianOption *ao = new AsianOption(T, H-1, size, coefs, strike);

    double price, ic = 0;
    MonteCarlo *mc = new MonteCarlo(mod, ao, rng, fdStep, nbSamples);

    double error = 10;
    PnlVect *pf = pnl_vect_create(H);

    PandL *pandl = new PandL();
    pandl->hedgingPortfolio(givenTraj, mc, pf, error);
    cout << endl << "error = " << error << endl;

    ASSERT_TRUE(error > -2 && error < 2);

    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    pnl_vect_free(&pf);

    pnl_mat_free(&givenTraj);

    delete P;
    delete mc;
    delete mod;
    delete ao;
    delete pandl;
}
