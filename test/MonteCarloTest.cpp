#include "Parser.hpp"
#include "MonteCarlo.hpp"
#include "BasketOption.hpp"
#include "AsianOption.hpp"
#include "PerformanceOption.hpp"
#include "BlackScholesModel.hpp"

#include "gtest/gtest.h"

using namespace std;

class MonteCarloTest : public ::testing::Test {
    protected:

        virtual void SetUp() {
            // code called before each test (beginning)
        }

        virtual void TearDown() {
            // code called before each test destructor (ending)
        }
};

TEST_F(MonteCarloTest, Prof_0) {

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../../resources/data-soutenance-sol/perf-withsol.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);
    //mod->asset(path, T, nbTimeSteps, rng);
    //pnl_mat_print(path);

    PerformanceOption *ao = new PerformanceOption(T, nbTimeSteps, size, coefs, strike);

    double price = 0, ic = 0;

    MonteCarlo *mc = new MonteCarlo(mod, ao, rng, fdStep, nbSamples);
    mc->price(price, ic);

    cout << "MONTECARLO" << endl;
    cout << "price = " << price << endl;
    cout << "ic = " << ic << endl;

    //ASSERT_TRUE((price > 4.67 - ic) && (price < 4,67 + ic));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc;
    delete ao;
    delete mod;
}

TEST_F(MonteCarloTest, DISABLED_Asian1_0) {

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian_1.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);
    //mod->asset(path, T, nbTimeSteps, rng);
    //pnl_mat_print(path);

    AsianOption *ao = new AsianOption(T, nbTimeSteps, size, coefs, strike);

    double price = 0, ic = 0;

    MonteCarlo *mc = new MonteCarlo(mod, ao, rng, fdStep, nbSamples);
    mc->price(price, ic);

    cout << "MONTECARLO" << endl;
    cout << "price = " << price << endl;
    cout << "ic = " << ic << endl;

    ASSERT_TRUE((price > 4.67 - ic) && (price < 4,67 + ic));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc;
    delete ao;
    delete mod;
}

TEST_F(MonteCarloTest, DISABLED_Basket_0) {

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BasketOption *bo = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    double price, ic = 0;
    mc->price(price, ic);

    cout << endl << "MONTECARLO" << endl;
    cout << "price = " << price << endl;
    cout << "ic = " << ic << endl << endl;

    ASSERT_TRUE((price > 6.37 - ic) && (price < 6.37 + ic));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc;
    delete bo;
    delete mod;
}

TEST_F(MonteCarloTest, DISABLED_Basket1_0) {

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_1.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    BasketOption *bo = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    double price, ic = 0;
    mc->price(price, ic);

    cout << "MONTECARLO" << endl;
    cout << "price = " << price << endl;
    cout << "ic = " << ic << endl;

    ASSERT_TRUE((price > 13.627 - ic) && (price < 13.627 + ic));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc;
    delete bo;
    delete mod;
}

TEST_F(MonteCarloTest, DISABLED_Basket2_0) {

    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/basket_2.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    /* MONTECARLO */

    BlackScholesModel *mod = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    BasketOption *bo = new BasketOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc = new MonteCarlo(mod, bo, rng, fdStep, nbSamples);

    double price, ic = 0;
    mc->price(price, ic);

    cout << "MONTECARLO" << endl;
    cout << "price = " << price << endl;
    cout << "ic = " << ic << endl;

    ASSERT_TRUE((price > 9.238710 - ic) && (price < 9.238710 + ic));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc;
    delete bo;
    delete mod;
}

TEST_F(MonteCarloTest, DISABLED_Asian_t_eq_0) {
    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    // PAST
    PnlMat *past = pnl_mat_create(1, size);
    pnl_mat_set_row(past, spot, 0);

    BlackScholesModel *bs1 = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    //cout << endl << "=== PAST ===" << endl;
    //pnl_mat_print(past);

    AsianOption *ao1 = new AsianOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc1 = new MonteCarlo(bs1, ao1, rng, fdStep, nbSamples);

    double price1, ic1 = 0;
    mc1->price(price1, ic1);

    cout << "MONTECARLO EN 0" << endl;
    cout << "price = " << price1 << endl;
    cout << "ic = " << ic1 << endl;

    // PATH
    //PnlMat *path = pnl_mat_create(nbTimeSteps+1, size);

    //BlackScholesModel *bs2 = new BlackScholesModel(size, r, rho, sigma, spot);

    //bs2->asset(path, 0, T+0, nbTimeSteps, rng, past);
    //bs1->asset(path, 0, T+0, nbTimeSteps, rng, past);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    AsianOption *ao2 = new AsianOption(T, nbTimeSteps, size, coefs, strike);

    //MonteCarlo *mc2 = new MonteCarlo(bs2, ao2, rng, fdStep, nbSamples);
    MonteCarlo *mc2 = new MonteCarlo(bs1, ao2, rng, fdStep, nbSamples);

    double price2 = 0, ic2 = 0;
    mc2->price(past, 0, price2, ic2);

    cout << endl << "MONTECARLO PATH" << endl;
    cout << "price = " << price2 << endl;
    cout << "ic = " << ic2 << endl << endl;

    ASSERT_TRUE((price2 > 4.67 - ic2) && (price2 < 4,67 + ic2));

    // Free
    pnl_rng_free(&rng);
    //pnl_mat_free(&path);
    pnl_mat_free(&past);
    pnl_vect_free(&spot);
    pnl_vect_free(&sigma);
    pnl_vect_free(&trend);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc2;
    delete ao2;
    delete mc1;
    delete ao1;
    delete bs1;
}

TEST_F(MonteCarloTest, DISABLED_Asian_t_eq_5) {
    /* BLACK-SCHOLES */

    int size; /// nombre d'actifs du modèle
    double r; /// taux d'intérêt
    double rho; /// paramètre de corrélation
    PnlVect *sigma; /// vecteur de volatilités
    PnlVect *spot; /// valeurs initiales du sous-jacent
    double T;

    /* OPTION */
    double strike;
    PnlVect *coefs;
    int nbTimeSteps;
    double fdStep = 1; /*! pas de différence finie */
    size_t nbSamples; /*! nombre de tirages Monte Carlo */
    string optionType;

    PnlVect *trend; /// tendance du modèle
    int H; /// nombre de dates de simulation

    // Parser
    string infile = "../../data/asian.dat";
    Param *P = new Parser(infile.c_str());

    P->extract("option type", optionType);
    P->extract("maturity", T);
    P->extract("option size", size);
    P->extract("strike", strike);
    P->extract("payoff coefficients", coefs, size);
    P->extract("sample number", nbSamples);
    P->extract("spot", spot, size);
    P->extract("volatility", sigma, size);
    P->extract("interest rate", r);
    P->extract("correlation", rho);
    P->extract("timestep number", nbTimeSteps);
    P->extract("trend", trend, size);
    P->extract("hedging dates number", H);

    /*cout << "option size " << size << endl;
    cout << "strike " << strike << endl;
    cout << "spot ";
    pnl_vect_print_asrow(spot);
    cout << "maturity " << T << endl;
    cout << "volatility ";
    pnl_vect_print_asrow(sigma);
    cout << "interest rate " << r << endl;
    cout << "correlation " << rho << endl;
    cout << "trend ";
    pnl_vect_print_asrow(trend);

    cout << endl << "option type " << optionType << endl;
    cout << "payoff coefficients ";
    pnl_vect_print_asrow(coefs);

    cout << endl << "timestep number " << nbTimeSteps << endl;
    cout << "sample number " << nbSamples << endl;
    cout << "hedging dates number " << H << endl;*/

    PnlRng *rng = pnl_rng_create(0);
    pnl_rng_sseed(rng, time(NULL));

    /* MONTECARLO */

    // PAST
    PnlMat *past = pnl_mat_create_from_scalar(6, size, 100.0);
    /*pnl_mat_set_row(past, spot, 0);

    pnl_mat_create_from_scalar(past,100.0)

    PnlVect *spotCopy = pnl_vect_create(size);
    pnl_vect_clone(spotCopy,spot);
    pnl_mat_set_row(past, spotCopy, 4);*/

    BlackScholesModel *bs1 = new BlackScholesModel(size, r, rho, sigma, spot, trend);

    //cout << endl << "=== PAST ===" << endl;
    //pnl_mat_print(past);

    AsianOption *ao1 = new AsianOption(T, nbTimeSteps, size, coefs, strike);

    MonteCarlo *mc1 = new MonteCarlo(bs1, ao1, rng, fdStep, nbSamples);

    double price1, ic1 = 0;
    mc1->price(price1, ic1);

    cout << endl << "= MONTECARLO EN O =" << endl;
    cout << "price = " << price1 << endl;
    cout << "ic = " << ic1 << endl << endl;

    // PATH
    PnlMat *path = pnl_mat_create(nbTimeSteps+6, size);

    //BlackScholesModel *bs2 = new BlackScholesModel(size, r, rho, sigma, spot);

    //bs2->asset(path, 0, T+0, nbTimeSteps, rng, past);
    bs1->asset(path, 5*(T / ((double)nbTimeSteps)), T+5*(T / ((double)nbTimeSteps)), nbTimeSteps+5, rng, past);

    //cout << endl << "=== PATH ===" << endl;
    //pnl_mat_print(path);

    AsianOption *ao2 = new AsianOption(T+5*(T / ((double)nbTimeSteps)), nbTimeSteps+5, size, coefs, strike);

    //MonteCarlo *mc2 = new MonteCarlo(bs2, ao2, rng, fdStep, nbSamples);
    MonteCarlo *mc2 = new MonteCarlo(bs1, ao2, rng, fdStep, nbSamples);

    double price2 = 0, ic2 = 0;
    mc2->price(past, 5*(T / ((double)nbTimeSteps)), price2, ic2);

    cout << "MONTECARLO PATH" << endl;
    cout << "price = " << price2 << endl;
    cout << "ic = " << ic2 << endl;

    ASSERT_TRUE((price2 > 4) && (price2 < 5));
    //Valeur inconnue à  ~4,4, dû à la prise en compte des premières valeurs dans le calcul d'AsianOption.

    // Free
    pnl_vect_free(&sigma);
    pnl_vect_free(&spot);
    pnl_vect_free(&trend);
    pnl_rng_free(&rng);
    pnl_mat_free(&past);
    pnl_mat_free(&path);
    pnl_vect_free(&coefs);
    
    delete P;
    delete mc2;
    delete ao2;
    delete mc1;
    delete ao1;
    delete bs1;
}
