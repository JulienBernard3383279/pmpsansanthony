#include <cmath>

#include "BasketOption.hpp"
#include "AsianOption.hpp"
#include "PerformanceOption.hpp"

#include "gtest/gtest.h"

using namespace std;

class OptionTest : public ::testing::Test {
    protected:

        virtual void SetUp() {
            // code called before each test (beginning)
        }

        virtual void TearDown() {
            // code called before each test destructor (ending)
        }
};

TEST_F(OptionTest, Basket) {
    PnlMat *pnlMatPointer = pnl_mat_create(2, 2);

    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 1.0, 2.0), 0);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 3.0, 2.0), 1);

    PnlVect *pnlVectPointer = pnl_vect_create_from_list(2, 0.8, 0.2);

    BasketOption bo(1.0, 1, 2, pnlVectPointer, 1.0); //T inutilisé pour payoff

    double d = bo.payoff(pnlMatPointer);
    // d = 0.8 * 3.0 + 0.2 * 2.0 = 2.8
    ASSERT_TRUE(abs(d - 1.8) < 1e-12);
}

TEST_F(OptionTest, Asian) {
    PnlMat *pnlMatPointer = pnl_mat_create(2, 2);

    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 2.0, 18.0), 0);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 3.0, 27.0), 1);

    PnlVect *pnlVectPointer = pnl_vect_create_from_list(2, 0.9, 0.1);

    AsianOption ao(1.0, 1, 2, pnlVectPointer, 1.0);

    double d = ao.payoff(pnlMatPointer); // d = (1,62 * 2) - 1.0
    ASSERT_TRUE(abs(d - 3.5) < 1e-12);
}

TEST_F(OptionTest, Performance) {
    PnlMat *pnlMatPointer = pnl_mat_create(3, 2);
    
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 10.0, 10.0), 0);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 0.0, 10.0), 1);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2, 30.0, 0.0), 2);
    
    PnlVect *pnlVectPointer = pnl_vect_create_from_list(2, 0.5, 0.5);
    PerformanceOption po(1.0, 2, 2, pnlVectPointer, 1.0);
    //T inutilisé
    double d = po.payoff(pnlMatPointer); // d = ( 1.0 + 0.0 + 2.0 - 1.0 )
    ASSERT_TRUE(abs(d - 2.0)<1e-12 );

}
