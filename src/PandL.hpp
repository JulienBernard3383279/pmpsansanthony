#pragma once

#include "pnl/pnl_vector.h"
#include "pnl/pnl_matrix.h"
#include "MonteCarlo.hpp"

class PandL {
public:    
    PandL();
    virtual ~PandL();
    /**
     * Construit le portefeuille de couverture
     *
     * @param[in] givenTraj contient une trajectoire du modèle.
     * C'est une matrice de taille (nbTimeSteps+1) x d
     * @param[in] mc Instance de MonteCarlo
     * @param[out] pf Portefeuille de couverture
     * @param[out] error Erreur de couverture
     */
  void hedgingPortfolio(PnlMat *givenTraj, MonteCarlo *mc, PnlVect *pf, double &error);
};
