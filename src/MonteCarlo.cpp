 /**
 * \file    MonteCarlo.cpp
 * \author  3A IF - Equipe 1
 * \version 1.0
 * \date    13/09/2017
 * \brief   Implémente la structure de la classe MonteCarlo
 *
 * \details
 */

#include "MonteCarlo.hpp"

using namespace std;

MonteCarlo::MonteCarlo() {}

MonteCarlo::MonteCarlo(BlackScholesModel *mod, Option *opt, PnlRng *rng, double fdStep, int nbSamples) {
    mod_ = mod;
    opt_ = opt;
    rng_ = rng;
    fdStep_ = fdStep;
    nbSamples_ = nbSamples;
}

MonteCarlo::~MonteCarlo() {}

void MonteCarlo::price(double &prix, double &ic) {

    double sum = 0;
    double squaredSum = 0;
    double var = 0;

    PnlMat *path = pnl_mat_create(opt_->nbTimeSteps+1, mod_->size_);

    mod_->initAsset(opt_->nbTimeSteps);
    for (int i = 0; i < nbSamples_; ++i) {
        mod_->postInitAsset(path, opt_->T, opt_->nbTimeSteps, rng_);

        sum += opt_->payoff(path);
        squaredSum = squaredSum + pow(opt_->payoff(path), 2);
    }

    prix = sum/nbSamples_ * exp(-mod_->r_ * opt_->T);

    var = exp(-mod_->r_ * opt_->T * 2)
            * (squaredSum/nbSamples_ - pow(sum/nbSamples_, 2));

    ic = 2 * 1.96 * sqrt(var) / sqrt(nbSamples_);

    // Free memory
    pnl_mat_free(&path);
}

/**
 * @brief Calcule le prix de l'option à la date t
 */


void MonteCarlo::price(const PnlMat *past, double t, double &prix, double &ic) {
    double temp = 0, sum = 0, squaredSum = 0, var = 0;

    PnlMat *path = pnl_mat_create(opt_->nbTimeSteps+1, mod_->size_);

    mod_->initAsset(opt_->nbTimeSteps);
    
    for (int i = 0; i < nbSamples_; ++i) {
        mod_->postInitAsset(path, t, opt_->T, opt_->nbTimeSteps, rng_, past);
        temp = opt_->payoff(path);
        sum += temp;
        squaredSum = squaredSum + pow(temp, 2);
    }

    prix = exp(-mod_->r_ * (opt_->T - t)) * sum/nbSamples_;

    var = exp(-mod_->r_ * (opt_->T - t) * 2)
            * (squaredSum/nbSamples_ - pow(sum/nbSamples_, 2));

    ic = 2 * 1.96 * sqrt(var) / sqrt(nbSamples_);

    // Free memory
    pnl_mat_free(&path);
}

/**
 * @brief Calcule le delta de l'option à la date t
 */
void MonteCarlo::delta(const PnlMat *past, double t, PnlVect *delta) {
    double h = 10e-3;
    double timestep = opt_->T / opt_->nbTimeSteps;

    PnlMat *path = pnl_mat_create(opt_->nbTimeSteps+1, mod_->size_);
    PnlMat *shift_path = pnl_mat_create(opt_->nbTimeSteps+1, mod_->size_);

    pnl_vect_set_all(delta, 0);

    mod_->initAsset(opt_->nbTimeSteps);
    for (int j = 0; j < nbSamples_; ++j) {
        // Simulation de Monte Carlo
        mod_->postInitAsset(path, t, opt_->T, opt_->nbTimeSteps, rng_, past);

        // Calcul des payoffs
        for (int d = 0; d < mod_->size_; ++d) {
            mod_->shiftAsset(shift_path, path, d, h, t, timestep);
            double sum = opt_->payoff(shift_path);

            mod_->shiftAsset(shift_path, path, d, -h, t, timestep);
            sum -= opt_->payoff(shift_path);

            LET(delta, d) += sum;
        }
    }

    for (int d = 0; d < mod_->size_; ++d) {
        LET(delta, d) *= exp(-mod_->r_ * (opt_->T - t))
                / (nbSamples_ * 2 * MGET(past, past->m-1, d) * h);
    }

    // Free memory
    pnl_mat_free(&path);
    pnl_mat_free(&shift_path);
}
