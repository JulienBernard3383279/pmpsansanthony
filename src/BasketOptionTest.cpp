#include "BasketOption.hpp"
#include "pnl/pnl_matrix.h"
#include "pnl/pnl_vector.h"
#include <iostream>

int main (int argc, char *argv[]) {
    PnlMat *pnlMatPointer = pnl_mat_create(2,2);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2,  1,3), 0);
    pnl_mat_set_row(pnlMatPointer, pnl_vect_create_from_list(2,  2,2), 1);
    PnlVect *pnlVectPointer = pnl_vect_create_from_list(2,  0.8,0.2);
    BasketOption bo(pnlVectPointer, 1);
    bo.T = 1; bo.nbTimeSteps = 1; bo.size = 2;
    double d = bo.payoff(pnlMatPointer);
    std::cout << d;

    pnl_mat_free(&pnlMatPointer);

    return EXIT_SUCCESS;
}
