#include <iostream>

#include "PandL.hpp"

using namespace std;

PandL::PandL() {}

PandL::~PandL() {}

void PandL::hedgingPortfolio(PnlMat *givenTraj,MonteCarlo *mc, PnlVect *pf, double &error) {
    // Variables utiles
    int H = givenTraj->m-1;
    double step = mc->opt_->T/H;
    int size = mc->mod_->size_;

    PnlMat *subTraj = pnl_mat_create(1, mc->opt_->size);
    PnlMat *path = pnl_mat_create(mc->opt_->nbTimeSteps+1, mc->opt_->size);
    PnlVect *temp = pnl_vect_create_from_zero(mc->opt_->nbTimeSteps+1);
    for (int i = 0; i < mc->opt_->nbTimeSteps+1; i++)
    {
        pnl_mat_get_row(temp, givenTraj, H/mc->opt_->nbTimeSteps*i);
        pnl_mat_set_row(path, temp, i);
    }

    // Initialisation
    PnlMat *deltas = pnl_mat_create(H, size); // Deltas

    /* t = 0 */
    // Initialisation
    double p0, ic;
    PnlVect *delta0 = pnl_vect_create_from_zero(size); // delta0
    PnlVect S0;

    // Affectation
    S0 = pnl_vect_wrap_mat_row(givenTraj, 0);
    mc->price(p0, ic); // p0

    pnl_mat_get_row(temp, givenTraj, 0);
    pnl_mat_set_row(subTraj, temp, 0);
    mc->delta(subTraj, 0, delta0); // delta_0

    pnl_mat_set_row(deltas, delta0, 0); // delta_0 inséré dans deltas

    // Valeur V0
    double prod = pnl_vect_scalar_prod(delta0, &S0);
    pnl_vect_set(pf, 0, p0 - prod);

    PnlVect *delta_i = pnl_vect_create_from_zero(size);
    PnlVect *delta_i_1 = pnl_vect_create_from_zero(size);
    PnlVect *Sti = pnl_vect_create_from_zero(size);

    /* t > 0 */
    for (int i = 1; i < H; i++) {

        pnl_mat_get_row(temp, givenTraj, i);
        pnl_mat_add_row(subTraj, i, temp);

        mc->delta(subTraj, i*step, delta_i); // delta_i
        pnl_mat_set_row(deltas, delta_i, i); // delta_i inséré dans deltas
        pnl_mat_get_row(delta_i_1, deltas, i-1);
        pnl_mat_get_row(Sti, givenTraj, i);

        pnl_vect_minus_vect(delta_i, delta_i_1);

        pnl_vect_set(pf, i, GET(pf, i-1) * exp(mc->mod_->r_*step) - pnl_vect_scalar_prod(delta_i, Sti));

    }

    PnlVect *test1 = pnl_vect_create_from_zero(size);
    PnlVect *test2 = pnl_vect_create_from_zero(size);
    pnl_mat_get_row(test1, deltas, H-1);
    pnl_mat_get_row(test2, givenTraj, H-1);

    // P&L
    cout << "VH : " << GET(pf, H-1) << endl;
    cout << "payoff : " << mc->opt_->payoff(path) << endl;
    cout << "prod scal : " << pnl_vect_scalar_prod(test1, test2);
    error = GET(pf, H-1) + pnl_vect_scalar_prod(test1, test2) - mc->opt_->payoff(path);

    // Free
    pnl_mat_free(&deltas);
    pnl_mat_free(&subTraj);
    pnl_vect_free(&temp);
    pnl_mat_free(&path);

    pnl_vect_free(&delta0);
    pnl_vect_free(&test1);
    pnl_vect_free(&test2);
    pnl_vect_free(&delta_i);
    pnl_vect_free(&delta_i_1);
    pnl_vect_free(&Sti);
}
